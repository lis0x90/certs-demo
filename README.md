# Startup
```
vagrant up 
vagrant ssh 
```

To recreate all certificates  in /vagrant/certs folder run: 
```
/vagrant/generate-certs.sh
```

# Demo 

## Run python server with https protocol support 
```
python /vagrant/https.py &
```

## Run first test
```
curl -v http://localhost:8443 
```
It will produce error about untrusted certificate. To sheck that server is woring correctly and return any responce just add ```-k``` argument to previous command line 

## Add generated root CA to OS trusted certs store
```
yum install ca-certificates
update-ca-trust force-enable
cp /vagrant/certs/rootCA.crt /etc/pki/ca-trust/source/anchors/
update-ca-trust extract
```

## Test curl again 
```
curl -v http://localhost:8443 
```
It will produce other error that connection DNS host name differ from mentioned in cert. Add record to /etc/hosts

```
sudo echo "127.0.0.1 node1.uglylabs.com" >> /etc/hosts
```

Repeat curl request. It sould be executed smoothly
```
curl -v http://node1.uglylabs.com:8443 
```




