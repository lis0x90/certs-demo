import socket, ssl

context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
context.load_cert_chain(certfile="/vagrant/node1.uglylabs.com.crt", keyfile="/vagrant/node1.uglylabs.com.key")

bindsocket = socket.socket()
bindsocket.bind(('node1.uglylabs.com', 8443))
bindsocket.listen(5)


def deal_with_client(connstream):
    print("We have client!")
    data = connstream.recv(1024)
    # empty data means the client is finished with us
    while data:
        if not do_something(connstream, data):
            # we'll assume do_something returns False
            # when we're finished with client
            break
        data = connstream.recv(1024)
    # finished with client

while True:
    newsocket, fromaddr = bindsocket.accept()
    connstream = context.wrap_socket(newsocket, server_side=True)
    try:
        deal_with_client(connstream)
    finally:
        connstream.shutdown(socket.SHUT_RDWR)
        connstream.close()