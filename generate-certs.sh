#!/bin/bash

set -ex 

mkdir /vagrant/certs
rm -f /vagrant/certs/*.{key,csr,crt,pem,p12,srl}
cd /vagrant/certs

	
# create mythological certificate authority center 	
openssl genrsa -out rootCA.key 2048
openssl req -x509 -new -key rootCA.key -days 365 -out rootCA.crt -subj '/C=RU/ST=Moscow/L=Moscow/O=Kremlin/OU=FSB/CN=fsb.com'
cat rootCA.key rootCA.crt > rootCA.pem

# create target certificate for affiliated company 
# company private key 
openssl genrsa -out node1.uglylabs.com.key 2048
# request about sign company cetrificate 
openssl req -new -key node1.uglylabs.com.key -out node1.uglylabs.com.csr -subj '/C=BL/ST=Minsk/L=Minsk/O=Ambrella Comp/OU=Biological Weapon/CN=node1.uglylabs.com'

# sign company certificate by authority center 
echo '01' > rootCA.srl
openssl x509 -req -in node1.uglylabs.com.csr -CA rootCA.crt -CAkey rootCA.key -CAserial rootCA.srl -days 365 -out node1.uglylabs.com.crt

cat node1.uglylabs.com.key node1.uglylabs.com.crt > node1.uglylabs.com.pem
openssl pkcs12 -export -nodes -passout pass: -clcerts -in node1.uglylabs.com.crt -inkey node1.uglylabs.com.key -out node1.uglylabs.com.p12 

# client certificate 
openssl genrsa -out mila.key 2048
openssl req -new -key mila.key -nodes -out mila.csr -subj '/C=BL/ST=Minsk/CN=mila@node1.uglylabs.com/emailAddress=mila@node1.uglylabs.com'
echo '01' > node1.uglylabs.com.srl
# sign by ambrella
openssl x509 -req -CA node1.uglylabs.com.pem -CAserial node1.uglylabs.com.srl -in mila.csr -out mila.crt

cat mila.key mila.crt > mila.pem
openssl pkcs12 -nodes -passout pass: -export -clcerts -in mila.crt -inkey mila.key -out mila.p12

# create certs storage for apache 
cat node1.uglylabs.com.crt rootCA.crt > certs-bundle.crt

echo "Success!"