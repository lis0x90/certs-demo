import BaseHTTPServer, SimpleHTTPServer, ssl

httpd = BaseHTTPServer.HTTPServer(('localhost', 8443), SimpleHTTPServer.SimpleHTTPRequestHandler)
httpd.socket = ssl.wrap_socket(httpd.socket, certfile='/vagrant/certs/node1.uglylabs.com.pem', server_side=True)
httpd.serve_forever()